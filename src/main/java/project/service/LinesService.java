package project.service;

import com.mysql.cj.xdevapi.TableImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.dao.ILinesDAO;
import project.model.ResultData;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Component
public class LinesService implements ILinesService{
    @Autowired
    private ILinesDAO linesDAO;
    private Timestamp startTime;
    private Timestamp endTime;
    @Override
    public List<ResultData> getResultData(String currentLine, String date, int startHour, int endHour){
        List<ResultData> resultData = new ArrayList<>();
        for(int hour = startHour; hour <= endHour; hour++){
            ResultData data = new ResultData();
            data.setHour(hour);
            startTime = Timestamp.valueOf(date + " " + String.valueOf(hour) + ":00:00");
            endTime = Timestamp.valueOf(date + " " + String.valueOf(hour+1) + ":00:00");
            data.setIn(linesDAO.getCountByLine(currentLine, 1, startTime, endTime));
            data.setOut(linesDAO.getCountByLine(currentLine, 0, startTime, endTime));
            resultData.add(data);
        }
        return resultData;
    }

    @Override
    public List<String> getSceneIDs() {
        return linesDAO.getSceneIDs();
    }

    @Override
    public List<String> getLinesByScene(String sceneID) {
        return linesDAO.getLinesByScene(sceneID);
    }
}
