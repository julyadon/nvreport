package project.service;

import project.model.ResultData;

import java.util.List;

public interface ILinesService {
    List<String> getSceneIDs();
    List<String> getLinesByScene(String sceneID);
    List<ResultData> getResultData(String currentLine, String date, int startHour, int endHour);
}
