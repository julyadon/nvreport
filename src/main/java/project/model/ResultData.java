package project.model;

public class ResultData {
    int hour;
    long in;
    long out;

    public int getHour() {
        return hour;
    }

    public long getIn() {
        return in;
    }

    public long getOut() {
        return out;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setIn(long in) {
        this.in = in;
    }

    public void setOut(long out) {
        this.out = out;
    }
}
