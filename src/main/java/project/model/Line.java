package project.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.sql.Timestamp;

@Entity
@Table(name="line")
public class Line {
    @Id
    private int id;
    private String scene_id;
    private String lineTitle;
    private String uid;
    @PastOrPresent
    private Timestamp datetime;
    private int status;
    private int type;
    private Timestamp time_stamp;
    private boolean transmitted;
    private int taskID;

    public int getId() {
        return id;
    }

    public String getScene_id() {
        return scene_id;
    }

    public String getLineTitle() {
        return lineTitle;
    }

    public String getUid() {
        return uid;
    }

    public Timestamp getDatetime() {
        return datetime;
    }

    public int getStatus() {
        return status;
    }

    public int getType() {
        return type;
    }

    public Timestamp getTime_stamp() {
        return time_stamp;
    }

    public boolean isTransmitted() {
        return transmitted;
    }

    public int getTaskID() {
        return taskID;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public void setLineTitle(String lineTitle) {
        this.lineTitle = lineTitle;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setTime_stamp(Timestamp time_stamp) {
        this.time_stamp = time_stamp;
    }

    public void setTransmitted(boolean transmitted) {
        this.transmitted = transmitted;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }
}
