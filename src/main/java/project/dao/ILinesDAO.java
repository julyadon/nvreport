package project.dao;

import project.model.Line;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public interface ILinesDAO {
    List<String> getSceneIDs();
    List<String> getLinesByScene(String sceneID);
    Long getCountByLine(String lineTitle, int status, Timestamp startT, Timestamp endT);
}
