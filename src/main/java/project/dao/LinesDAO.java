package project.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.model.Line;

import javax.persistence.criteria.CriteriaBuilder;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Component
public class LinesDAO implements ILinesDAO{
    @Autowired
    private SessionFactory sessionFactory;

    private Session currentSession(){
        return sessionFactory.openSession();
    }

    @Override
    public List<String> getSceneIDs(){
        return currentSession().createQuery("select distinct scene_id from project.model.Line", String.class).list();
    }

    @Override
    public List<String> getLinesByScene(String sceneID){
        Query<String> q = currentSession().createQuery("select distinct lineTitle from project.model.Line where scene_id = :scene_id", String.class);
        q.setParameter("scene_id", sceneID);
        return q.list();
    }

    @Override
    public Long getCountByLine(String lineTitle, int status, Timestamp startT, Timestamp endT){
        Query<Line> q = currentSession().createQuery("select count (*) from project.model.Line where lineTitle = :lineTitle and status = :status and datetime between :startT and :endT");
        q.setParameter("lineTitle", lineTitle);
        q.setParameter("status", status);
        q.setParameter("startT", startT);
        q.setParameter("endT", endT);
        List data = q.list();
        Long in = Long.valueOf(0);
        if(data.size()>0){
            in = (Long)data.get(0);
        }
        return in;
    }
}
