package project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import project.dao.ILinesDAO;
import project.dao.LinesDAO;
import project.model.Line;
import project.model.Values;
import project.service.ILinesService;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Controller
public class MainController {
    @Autowired
    private ILinesService linesService;

    String currentScene;
    String currentLine;
    String date;
    String start;
    String end;
    @GetMapping("/")
    public String getStarted(){
        return "/index";
    }

    @GetMapping("/scenes")
    public String getScenes(Model model){
        model.addAttribute("scenes", linesService.getSceneIDs());
        return "/scenes";
    }

    @PostMapping("/scenes")
    public String scene(@ModelAttribute Line line){
        currentScene = line.getScene_id();
        return "redirect:/scenes/lines";
    }

    @GetMapping("/scenes/lines")
    public String getLines(Model model){
        model.addAttribute("values", new Values());
        model.addAttribute("lines", linesService.getLinesByScene(currentScene));
        return "/lines";
    }

    @PostMapping("/scenes/lines")
    public String lines(@ModelAttribute Line line, @ModelAttribute @Valid Values values, BindingResult result){
        currentLine = line.getLineTitle();
        if(result.hasErrors()){
            return "/lines";
        }
        date = values.getDate();
        start = values.getTimeStart();
        end = values.getTimeEnd();
        return "redirect:/scenes/lines/data";
    }

    @GetMapping("/scenes/lines/data")
    public String getData(Model model){
        model.addAttribute("data", linesService.getResultData(currentLine, date, Integer.valueOf(start), Integer.valueOf(end)));
        return "/result";
    }
}
