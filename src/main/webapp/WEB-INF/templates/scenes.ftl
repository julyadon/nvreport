<#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<html>
<head>
    <title>Home</title>
</head>
<body>
    <#if scenes?has_content>
        <@sf.form action="/scenes" method="post" modelAttribute="line">
            Available scenes:
        <select name = "scene_id">
            <#list scenes as scene>
                <option>${scene}</option>
            </#list>
        </select>
            <input type="submit" value="OK">
        </@sf.form>
    <#else>
    <p>No scenes</p>
    </#if>
</body>
</html>
