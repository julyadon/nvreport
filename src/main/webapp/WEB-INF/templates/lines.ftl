<#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<html>
<head>
    <title>Lines</title>
</head>
<body>
    <@sf.form action="/scenes/lines" method="post" modelAttribute="values">
        Available lines:
        <#if lines?has_content>
        <select name = "lineTitle">
            <#list lines as line>
                <option>${line}</option>
            </#list>
        </select>
        <div>
            <@sf.label path="date">Date:</@sf.label>
            <@sf.input path="date"/>
            <@sf.errors path="date"/>
        </div>

        Time from:
        <select name="timeStart">
            <#list 0..23 as hour>
                <option>${hour}</option>
            </#list>
        </select>

        to:
        <select name="timeEnd">
        <#list 0..23 as hour>
            <option>${hour}</option>
        </#list>
        </select>
        <input type="submit" value="OK">
        <#else>
            <p>No lines</p>
        </#if>
    </@sf.form>
</body>
</html>
