<#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<html>
<head>
    <title>Data</title>
</head>
<body>
    <@sf.form action="/scenes/lines" method="post">
        <#if data?has_content>
            <table style="padding: 10px;">
                <tr>
                    <th>Hour</th>
                    <th>In</th>
                    <th>Out</th>
                </tr>
                <#list data as d>
                <tr>
                    <td>${d.hour}</td>
                    <td>${d.in}</td>
                    <td>${d.out}</td>
                </tr>
                </#list>
            </table>
        <#else>
        <p>No data</p>
        </#if>
    </@sf.form>
</body>
</html>